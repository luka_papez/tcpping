package hr.corvus.tcpping;

import java.io.IOException;
import java.net.InetAddress;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

/**
 * Main entry point into the program.
 * 
 * @author Luka Papež
 * 
 */
public class TCPPing {

	// the values are initially uninitialized
	private static Boolean pitcher = null;
	private static Integer port = null;
	private static String bind = null;
	private static Integer mps = 1;
	private static Integer size = 300;
	private static String hostname = null;

	/**
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		// obtain the time offset from this computer to central time
		String TIME_SERVER = "hr.pool.ntp.org";
		NTPUDPClient timeClient = new NTPUDPClient();
		InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
		TimeInfo timeInfo = timeClient.getTime(inetAddress);
		timeInfo.computeDetails();
		long clockOffset = timeInfo.getDelay();
		// parse the parameters
		boolean goodInput = parseParameters(args);
		
		// exit on bad input
		if (!goodInput) {
			// print help
			usage();
			return;
		}

		// prepare execution
		Runnable worker;
		if (pitcher) {
			// if we want a pitcher mode
			worker = new Pitcher(port, mps, size, hostname, clockOffset);
		} else {
			// if we want a catcher mode
			worker = new Catcher(port, bind, clockOffset);
		}

		// run the job in new thread
		new Thread(worker).start();
	}

	/**
	 * Print the help section on wrong input.
	 */
	private static void usage() {
		System.out.println("Run with following args:");
		System.out.println("-p run as pitcher");
		System.out.println("-c run as catcher");
		System.out.println("-port <port> use this port number");
		System.out.println("[Catcher] -bind <bind> use this adress for bind");
		System.out
				.println("[Pitcher] -mps <rate> send this many messages per second");
		System.out.println("[Pitcher] -size <size> lenght of sent message");
		System.out.println("[Pitcher] <hostname> adress of the catcher");

	}

	/**
	 * Parse the command line parameters into appropriate fields.
	 * 
	 * @param args
	 *            command line arguments
	 * @return true on good input, false otherwise
	 */
	private static boolean parseParameters(String[] args) {
		// create the command line parser
		CommandLineParser parser = new DefaultParser();

		// create the Options
		Options options = new Options();
		options.addOption("p", false, "This is a pitcher");
		options.addOption("c", false, "This is a catcher");
		options.addOption("port", true, "Port adress");
		options.addOption("bind", true, "Bind adress, for Catcher only");
		options.addOption("mps", true, "Messages per second");
		options.addOption("size", true, "Size of the message");

		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args);

			// determine the mode of running
			if (line.hasOption("p")) {
				// it's a pitcher
				pitcher = true;
			} else if (line.hasOption("c")) {
				// it's a catcher
				pitcher = false;
			} else {
				// invalid input
				System.out
						.println("Mode of operation wasn't given. Please input either -p to work as pitcher, or -c to work as catcher.");
				return false;
			}

			// determine port number
			if (line.hasOption("port")) {
				try {
					port = Integer.parseInt(line.getOptionValue("port"));
					if (port < 1024 || port > 65535) {
						System.out
								.println("Port must be in range [1024, 65535], you entered: "
										+ port);
						return false;
					}
				} catch (NumberFormatException e) {
					System.out
							.println("Port number wasn't given. Please input port number as \"-port <portNumber>\"");
					return false;
				}

			} else {
				// port number wasn't given
				System.out
						.println("Port number wasn't given. Please input port as \"-port <portNumber>\"");
				return false;
			}

			if (!pitcher) {
				// if it's a catcher determine bind address
				if (line.hasOption("bind")) {
					bind = line.getOptionValue("bind");
				} else {
					// port number wasn't given
					System.out
							.println("Catcher bind adress wasn't given. Please input address as \"-bind <adress>\"");
					return false;
				}
			} else {
				// else it's a pitcher determine mps, size and hostname

				// MPS
				try {
					if (line.hasOption("mps")) {
						mps = Integer.parseInt(line.getOptionValue("mps"));
					}

					if (mps < 0) {
						System.out
								.println("Cannot send a negative number of messages. Please enter a positive number, you entered: "
										+ mps);
						return false;
					}
				} catch (NumberFormatException e) {
					System.out
							.println("Invalid number of messages per second. Did you input a positive integer?");
					return false;
				}

				// SIZE
				try {
					if (line.hasOption("size")) {
						size = Integer.parseInt(line.getOptionValue("size"));
					}

					if (size < 50 || size > 3000) {
						System.out
								.println("Cannot send a message of that size. Please enter a number in range [50, 3000], you entered: "
										+ size);
						return false;
					}
				} catch (NumberFormatException e) {
					System.out
							.println("Invalid message size. Did you input a positive integer?");
					return false;
				}

				// HOSTNAME
				if (line.getArgList().size() == 1) {
					hostname = line.getArgList().get(0);
				} else {
					System.out
							.println("Did you forget to input catcher host address?");
					return false;
				}
			}

			return true;

		} catch (Exception exp) {
			System.out.println("Unexpected exception:" + exp.getMessage());
		}

		return false;

	}

}
