package hr.corvus.tcpping;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class represents a pitcher in TCPPing. When run it will open a
 * connection to catcher and start sending messages. It also constantly listens
 * for pitcher replies and makes statistical measurments of RTT and network
 * delay.
 * 
 * The messages are in format: 1. int. messageSize 4 bytes, 2. int.
 * messageNumber 4 bytes, 3. long. timeSent 8 bytes, 4. byte[] padding to
 * required size
 * 
 * @author luka
 * 
 */
public class Pitcher implements Runnable {

	private final int port;
	private final int mps;
	private final int size;
	private final String hostname;

	private final long clockOffset;
	private final byte[] buffer = new byte[3000];
	private Socket socket;
	private int currentMessageNumber = 0;
	private final int timeDelay = 1000;
	private DataInputStream in;
	private DataOutputStream out;
	private int padding;
	private final int timeout = 5000;

	private long[] timesSent;
	private long[] timesReplied;
	private long[] timesReceived;
	private final int conversionFactor = 1000000;

	public Pitcher(Integer port, Integer mps, Integer size, String hostname,
			long clockOffset) {
		this.port = port;
		this.mps = mps;
		this.size = size;
		this.hostname = hostname;
		this.clockOffset = clockOffset;
		this.padding = size - 2 * Integer.SIZE / 8 - Long.SIZE / 8;
		this.timesSent = new long[mps];
		this.timesReplied = new long[mps];
		this.timesReceived = new long[mps];
	}

	@Override
	public void run() {
		System.out.println("Running pitcher.");

		// open connection to catcher
		try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(hostname, port), timeout);
			in = new DataInputStream(new BufferedInputStream(
					socket.getInputStream()));
			out = new DataOutputStream(new BufferedOutputStream(
					socket.getOutputStream()));
		} catch (IOException e) {
			System.out.println("Couldn't run pitcher.");
			e.printStackTrace();
			return;
		}

		// start receive thread
		new Thread(new ReceiveTask()).start();

		// schedule sending and statistics
		Timer timer = new Timer();
		timer.schedule(new SendTask(timer), 0, timeDelay / mps);
		timer.schedule(new StatsTask(), timeDelay - 1, timeDelay);

	}

	private class SendTask extends TimerTask {
		Timer timer;

		public SendTask(Timer timer) {
			this.timer = timer;
		}

		@Override
		public void run() {
			try {

				// write the current message
				long time = System.nanoTime() + clockOffset;
				timesSent[currentMessageNumber % mps] = time;
				out.writeInt(size);
				out.writeInt(currentMessageNumber++);
				out.writeLong(time);
				out.write(buffer, 0, padding);
				out.flush();

				/*
				 * System.err.println("Pitcher sending: " + size + ", " +
				 * (currentMessageNumber - 1) + " at " + time);
				 */

			} catch (IOException e) {
				System.out.println("Pitcher network error.");
				timer.cancel();
			}
		}
	}

	private class ReceiveTask implements Runnable {

		@Override
		public void run() {
			try {
				while (true) {
					// receive reply
					int sizeReplied = in.readInt();
					int numberReplied = in.readInt();
					long timeReplied = in.readLong();
					long timeNow = System.nanoTime() + clockOffset;

					timesReplied[numberReplied % mps] = timeReplied;
					timesReceived[numberReplied % mps] = timeNow;

					int padding = sizeReplied - 2 * Integer.SIZE / 8
							- Long.SIZE / 8;
					in.skipBytes(padding);

					/*
					 * System.err.println("Pitcher received: " + sizeReplied +
					 * ", " + numberReplied + " at " + timeReplied);
					 */

				}
			} catch (IOException e) {
				System.out.println("Pitcher network error.");
			}
		}
	}

	private class StatsTask extends TimerTask {
		@Override
		public void run() {
			double avgAToB = calcAvgAToB();
			double avgBToA = calcAvgBToA();
			double avgRTT = calcAvgRTT();
			double maxAToB = calcMaxAToB();
			double maxBToA = calcMaxBToA();
			double maxRTT = calcMaxRTT();
			System.out.println(new Date(System.currentTimeMillis()).toString()
					+ ", sent: " + currentMessageNumber + ", mps: " + mps
					+ ", A->B: " + avgAToB + ", B->A: " + avgBToA + ", RTT: "
					+ avgRTT + ", max(A->B): " + maxAToB + ", max(B->A): "
					+ maxBToA + ", max(RTT): " + maxRTT);

		}

		private double calcMaxRTT() {
			long max = 0;
			for (int i = 0; i < mps; i++) {
				if (Math.abs((timesReplied[i] - timesSent[i])) > max) {
					max = Math.abs(timesReceived[i] - timesSent[i]);
				}
			}

			return max / conversionFactor;
		}

		private double calcMaxBToA() {
			long max = 0;
			for (int i = 0; i < mps; i++) {
				if (Math.abs((timesReplied[i] - timesSent[i])) > max) {
					max = Math.abs(timesReceived[i] - timesReplied[i]);
				}
			}

			return max / conversionFactor;
		}

		private double calcMaxAToB() {
			long max = 0;
			for (int i = 0; i < mps; i++) {
				if (Math.abs((timesReplied[i] - timesSent[i])) > max) {
					max = Math.abs(timesReplied[i] - timesSent[i]);
				}
			}

			return max / conversionFactor;
		}

		private double calcAvgRTT() {
			long sum = 0;
			for (int i = 0; i < mps; i++) {
				sum += Math.abs(timesReceived[i] - timesSent[i]);
			}

			return sum / conversionFactor / mps;
		}

		private double calcAvgBToA() {
			long sum = 0;
			for (int i = 0; i < mps; i++) {
				sum += Math.abs(timesReceived[i] - timesReplied[i]);
			}

			return sum / conversionFactor / mps;
		}

		private double calcAvgAToB() {
			long sum = 0;
			for (int i = 0; i < mps; i++) {
				sum += Math.abs(timesReplied[i] - timesSent[i]);
			}

			return sum / conversionFactor / mps;
		}
	}
}
