package hr.corvus.tcpping;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class represents a catcher in TCPPing. When run, it will wait a first
 * connection on given port and then receive and return pings.
 * 
 * The messages are in format: 1. int. messageSize 4 bytes, 2. int.
 * messageNumber 4 bytes, 3. long. timeSent 8 bytes, 4. byte[] padding to
 * required size
 * 
 * @author luka
 * 
 */
public class Catcher implements Runnable {

	private int port;
	private byte[] buffer = new byte[3000];
	private long clockOffset;
	private ServerSocket serverSocket;

	public Catcher(Integer port, String bind, long clockOffset) {
		this.port = port;
		this.clockOffset = clockOffset;
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("Couldn't run catcher.");
		}

	}

	@Override
	public void run() {

		// do indefinitely
		while (true) {
			System.out.println("Catcher awaiting new connection.");

			// await for a pitcher to connect
			try (Socket socket = serverSocket.accept()) {
				System.out.println("Pitcher connected from adress: "
						+ socket.getInetAddress().getHostAddress()
						+ " at port: " + port);

				// open connection
				DataInputStream in = new DataInputStream(
						new BufferedInputStream(socket.getInputStream()));
				DataOutputStream out = new DataOutputStream(
						socket.getOutputStream());

				// read and send messages
				while (true) {
					int messageSize = in.readInt();
					int messageNumber = in.readInt();
					// time received not needed here
					long timeReceived = in.readLong();
					int padding = messageSize - 2 * Integer.SIZE / 8
							- Long.SIZE / 8;
					in.skipBytes(padding);

					/*
					 * System.err.println("Catcher received: " + messageSize +
					 * ", " + messageNumber + " at " + timeReceived);
					 */

					long timeSending = System.nanoTime() + clockOffset;
					out.writeInt(messageSize);
					out.writeInt(messageNumber);
					out.writeLong(timeSending);
					out.write(buffer, 0, padding);
					out.flush();

					/*
					 * System.err.println("Catcher sending: " + messageSize +
					 * ", " + messageNumber + " at " + timeSending);
					 */

				}

			} catch (IOException e) {
				System.out.println("Connection closed.");
			}
		}
	}
}
