Instructions:
1. - Build with Maven ("mvn clean package shade:shade");
2. - Execute on two different computers with access to Internet.
  (java -classpath ./bin/:target/TCPPing-0.0.2-SNAPSHOT.jar hr.corvus.tcpping.TCPPing <options>)

Options are:

Catcher > –c –bind <ip-address> –port <port>

Pitcher > –p –port <port> [–mps <num_of_mess>] [–size <packet_size>] <hostname>

Args are:
-p run as pitcher
-c run as catcher
-port <port> use this port number
[Catcher] -bind <bind> use this adress for bind
[Pitcher] -mps <rate> send this many messages per second
[Pitcher] -size <size> lenght of sent message
[Pitcher] <hostname> adress of the catcher
